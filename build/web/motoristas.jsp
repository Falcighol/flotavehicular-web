<%-- 
    Document   : motoristas
    Created on : 3/11/2018, 02:28:31 PM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
        if (idTu == 2)
        {
            response.sendRedirect("index.jsp?denied=true");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="Ies del sistema">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Vista de vehiculos del sistema">
    <meta name="author" content="Juan Pablo Elias Hernández">
    <title>Motoristas - Flota</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="img/icon.png" sizes="64x64">
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
        <a class="navbar-brand" href="index.jsp"><i class="fas fa-truck"></i> Flota Vehicular</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link text-center" href="index.jsp">
                        <i class="fa fa-fw fa-home"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Vehiculos">
                    <a class="nav-link text-center" href="vehiculos.jsp">
                        <i class="fa fa-fw fa-car"></i>
                        <span class="nav-link-text">Vehiculos</span>
                    </a>
                </li>
                <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Motoristas">
                    <a class="nav-link text-center" href="motoristas.jsp">
                        <i class="fa fa-fw fa-users"></i>
                        <span class="nav-link-text">Motoristas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Viajes">
                    <a class="nav-link text-center" href="viajes.jsp">
                        <i class="fa fa-fw fa-luggage-cart"></i>
                        <span class="nav-link-text">Viajes</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Rutas">
                    <a class="nav-link text-center" href="rutas.jsp">
                        <i class="fa fa-fw fa-map-signs"></i>
                        <span class="nav-link-text">Rutas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clientes">
                    <a class="nav-link text-center" href="clientes.jsp">
                        <i class="fa fa-fw fa-address-book"></i>
                        <span class="nav-link-text">Clientes</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Mantenimientos">
                    <a class="nav-link text-center" href="mantenimientos.jsp">
                        <i class="fa fa-fw fa-cogs"></i>
                        <span class="nav-link-text">Mantenimientos</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Talleres">
                    <a class="nav-link text-center" href="talleres.jsp">
                        <i class="fa fa-fw fa-wrench"></i>
                        <span class="nav-link-text">Talleres</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav sidenav-toggler">
                <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" align="center">
                    <button class="btn btn-light btn-sm" data-toggle="modal" data-target="#logoutModal">
                        <i class="fa fa-fw fa-sign-out-alt"></i> 
                        Cerrar sesión
                    </button>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Content -->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Header -->
            <div class="row">
                <div class="col-12">
                    <br>
                    <h1 align="center" class="text-dark">
                        <i class="fa fa-fw fa-users"></i> 
                        LISTA DE MOTORISTAS
                    </h1>
                    <hr class="col-10">
                    <br>
                </div>
            </div>
            <!-- Breadcrumb -->
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.jsp">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Motoristas</li>
                    </ol>
                </div>
            </div>
            <br>
            <!-- DataTable Card -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Listado de motoristas
                    <button class="btn btn-light btn-sm" id="refreshTable" 
                            style="float: right;" data-toggle="tooltip" 
                            data-placement="top" title="Actualizar">
                        <i class="fa fa-fw fa-sync-alt"></i>
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellido</th>
                                    <th scope="col">Edad</th>
                                    <th scope="col">Genero</th>
                                    <th scope="col">DUI</th>
                                    <th scope="col">Licencia</th>
                                    <th scope="col">Dirección</th>
                                    <th scope="col">Teléfono</th>
                                    <th scope="col">Observaciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellido</th>
                                    <th scope="col">Edad</th>
                                    <th scope="col">Genero</th>
                                    <th scope="col">DUI</th>
                                    <th scope="col">Licencia</th>
                                    <th scope="col">Dirección</th>
                                    <th scope="col">Teléfono</th>
                                    <th scope="col">Observaciones</th>
                                </tr>
                            </tfoot>
                            <tbody id="tableContent">

                            </tbody>
                        </table>
                    </div>
                </div>
            <div class="card-footer small text-muted">
                <span id="tableUpdateTime"></span>
            </div>
        </div>
        </div>
        <!-- Footer -->
        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © Flota Vehicular 2018</small>
                </div>
            </div>
        </footer>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <i class="fa fa-fw fa-sign-out-alt"></i>
                            Cerrar sesión
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Todos los cambios no guardados se perderan!
                        Si está seguro de cerrar la sesión actual, seleccione la opcion <b>Salir</b>.
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">
                            <i class="fa fa-fw fa-undo"></i>
                            Cancelar
                        </button>
                        <button class="btn btn-outline-dark" id="logout">
                            <i class="fa fa-fw fa-sign-out-alt"></i>
                            Salir
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery-3.3.1.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/font-awesome/js/all.js"></script>
    <script src="js/jquery.mask.js" type="text/javascript"></script>
    <script src="js/sweetalert2.all.min.js" type="text/javascript"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/motoristas.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.js"></script>
</body>
</html>