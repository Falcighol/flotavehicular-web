var table;
(function($) 
{   
    setInterval('all()', 20000);
    
    if ($(document).width() > 800)
    {
        $("body").toggleClass("sidenav-toggled");
        $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
        $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
    }
    
    all();
    
    table = dt();
    
    $('.alphaonly').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z]/g,'') ); 
    });

    $("#new").on('click', function (e)
    {
        clean();
    });

    $("#refreshTable").on('click', function (e)
    {
        all();
    });
    
    $("#btnAdd").on('click', function (e)
    {
        var form = $('#frmAdd')[0];
        var km = $('#km').val();
        var or = $('#or').val();
        var des = $('#des').val();
        var desc = $('#desc').val();
        add(form, e, km, or, des, desc);
    });
    
    $("#btnEdit").on('click', function (e)
    {
        var form = $('#frmEdit')[0];
        var id = $('#idEdit').val();
        var km = $('#kmEdit').val();
        var or = $('#orEdit').val();
        var des = $('#desEdit').val();
        var desc = $('#descEdit').val();
        edit(form, e, id, km, or, des, desc);
    });
    
    $("#btnDel").on('click', function (e)
    {
        var id = $('#idDel').val();
        $('#deleteModal').modal('hide');
        del(id);
    });

})(jQuery);

function add (form, e, km, or, des, desc)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        $('#addModal').modal('hide');
        let datos = {
            'action' : 'add',
            'id' : 0,
            'km' : km,
            'or' : or,
            'des' : des,
            'desc' : desc
        };
        $.post("controllerRutas", datos, function (res)
        {
            var data = JSON.parse(res);
            if (!data.error)
            {
                swal({
                    title: 'Registro guardado!',
                    text: "La transaccón se ha realizado exitosamente.",
                    type: 'success'
                });
                all();
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al guardar!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
    }
    form.classList.add('was-validated');
}

function edit (form, e, id, km, or, des, desc)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        $('#editModal').modal('hide');
        swal({
            title: 'Seguro que desea editar el registro?',
            text: "Si selecciona 'Aceptar', la transaccion será realizada.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#138496',
            cancelButtonColor: '#5A6268',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value)
            {
                let datos = {
                    'action' : 'edit',
                    'id' : id,
                    'km' : km,
                    'or' : or,
                    'des' : des,
                    'desc' : desc
                };
                $.post("controllerRutas", datos, function (res)
                {
                    var data = JSON.parse(res);
                    if (!data.error)
                    {
                        swal({
                            title: 'Registro actualizado!',
                            text: "La transaccón se ha realizado exitosamente.",
                            type: 'success'
                        });
                        all();
                    }
                    else if (data.error)
                    {
                        swal({
                            title: 'Error al editar!',
                            text: "Algo salio mal en la transaccion: " + data.info,
                            type: 'error'
                        });
                    }
                });
            }
        });
    }
    form.classList.add('was-validated');
}

function del (id)
{
    swal({
        title: 'Seguro que desea eliminar el registro?',
        text: "Si selecciona 'Eliminar', la transaccion será realizada.",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#C82333',
        cancelButtonColor: '#5A6268',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            $.post("controllerRutas", 
            { 
                'action' : 'delete',
                'id' : id 
            }, 
            function (res)
            {
                var data = JSON.parse(res);
                if (!data.error)
                {
                    swal({
                        title: 'Registro eliminado!',
                        text: "La transaccón se ha realizado exitosamente.",
                        type: 'success'
                    });
                    all();
                }
                else if (data.error)
                {
                    swal({
                        title: 'Error al eliminar!',
                        text: "Algo salio mal en la transaccion: " + data.info,
                        type: 'error'
                    });
                }
            });
        }
    });
}

function all ()
{
    $.post("controllerRutas", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                $.each(JSON.parse(data.all), function(i, item) {
                    table.row.add([
                        item.kilometraje, 
                        item.origen, 
                        item.destino, 
                        item.descripcion,
                        `<button class="btn btn-info btn-sm" data-toggle="modal" 
                                data-target="#editModal" 
                                onclick="javascript:load(` + item.id + `, ` + item.kilometraje + `, '` 
                                + item.origen + `', '` + item.destino + `', '` 
                                + item.descripcion + `')">
                            <i class="fa fa-edit"></i>
                        </button>   
                        <button class="btn btn-danger btn-sm" data-toggle="modal" 
                                data-target="#deleteModal" onclick="javascript:loadId(` + item.id + `)">
                            <i class="fas fa-fw fa-trash-alt"></i>
                        </button>`
                    ]).draw();
                });
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
    let cd = new Date(); 
    let dt = "Ultima actualización " + cd.getDate() + "/"
            + (cd.getMonth()+1)  + "/" 
            + cd.getFullYear() + " a las "  
            + cd.getHours() + ":"  
            + cd.getMinutes() + ":" 
            + cd.getSeconds();
    $('#tableUpdateTime').html(dt);
//    console.log(dt);
}

function load(id, km, or, des, desc)
{
    $('#idEdit').val(id);
    $('#kmEdit').val(km);
    $('#orEdit').val(or);
    $('#desEdit').val(des);
    $('#descEdit').val(desc);
}

function loadId(id)
{
    $('#idDel').val(id);
}

function clean()
{
    $('#km').val('');
    $('#or').val('');
    $('#des').val('');
    $('#desc').val('');
}

function dt()
{
    return $("#dataTable").DataTable({
        "language": {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrando de un total de _MAX_ registros)",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "sSearch": "Buscar:",
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}