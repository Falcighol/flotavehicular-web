package com.models;

/**
 * @className Mantenimientos
 * @fecha 3/10/2018
 * @version 1.0
 * @copyright ITCA-FEPADE
 * @autor Juan Pablo Hernandez
 */
public class Mantenimientos
{
    private Integer id;
    private Vehiculo vehiculo;
    private Talleres taller;
    private String tipoMantenimiento;
    private String fechaEntrada;
    private String fechaSalida;
    private String fallaPrincipal;
    private String fallaSecundaria;

    public Mantenimientos() {
    }

    public Mantenimientos(Integer id, Vehiculo idVehiculo, Talleres idTaller, String tipoMantenimiento, String fechaEntrada, String fechaSalida, String fallaPrincipal, String fallaSecundaria) {
        this.id = id;
        this.vehiculo = idVehiculo;
        this.taller = idTaller;
        this.tipoMantenimiento = tipoMantenimiento;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
        this.fallaPrincipal = fallaPrincipal;
        this.fallaSecundaria = fallaSecundaria;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Talleres getTaller() {
        return taller;
    }

    public void setTaller(Talleres taller) {
        this.taller = taller;
    }

    public String getTipoMantenimiento() {
        return tipoMantenimiento;
    }

    public void setTipoMantenimiento(String tipoMantenimiento) {
        this.tipoMantenimiento = tipoMantenimiento;
    }

    public String getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(String fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getFallaPrincipal() {
        return fallaPrincipal;
    }

    public void setFallaPrincipal(String fallaPrincipal) {
        this.fallaPrincipal = fallaPrincipal;
    }

    public String getFallaSecundaria() {
        return fallaSecundaria;
    }

    public void setFallaSecundaria(String fallaSecundaria) {
        this.fallaSecundaria = fallaSecundaria;
    }
    
}
