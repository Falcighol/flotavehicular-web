
package com.models;

import com.connection.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de la clase: DaoRutas
 * Fecha: 12/10/2018
 * Version: 1.0
 * Copyright: Alvaro Perez
 * Autor: Alvaro Perez
 */
public class DaoRutas extends Conexion{
    
    public List all() throws Exception
    {
        List<Rutas> lstRutas = new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql = "SELECT * FROM Rutas WHERE estado = 1 ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res = pst.executeQuery();
            while(res.next())
            {
                Rutas ruta = new Rutas();
                ruta.setId(res.getInt(1));
                ruta.setKilometraje(res.getDouble(2));
                ruta.setOrigen(res.getString(3));
                ruta.setDestino(res.getString(4));
                ruta.setDescripcion(res.getString(5));
                ruta.setEstado(res.getInt(6));
                lstRutas.add(ruta);
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
            return lstRutas;
    }

    public void add(Rutas ruta) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "INSERT INTO Rutas VALUES (NULL, ?, ?, ?, ?, 1);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setDouble(1, ruta.getKilometraje());
            pst.setString(2, ruta.getOrigen());
            pst.setString(3, ruta.getDestino());
            pst.setString(4, ruta.getDescripcion());
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void edit(Rutas ruta) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Rutas SET kilometraje = ?, origen = ?, "
                    + "destino = ?, descripcion = ? WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setDouble(1, ruta.getKilometraje());
            pst.setString(2, ruta.getOrigen());
            pst.setString(3, ruta.getDestino());
            pst.setString(4, ruta.getDescripcion());
            pst.setInt(5, ruta.getId());
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void delete(Rutas ruta) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Rutas SET estado = 0 WHERE id = ?";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, ruta.getId());
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
