package com.models;

import com.connection.Conexion;
import com.models.Vehiculo;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: DaoVehiculo
 * Version: 1.0
 * Fecha: 3/10/2018
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernandez
 */
public class DaoVehiculo extends Conexion
{
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public List all() throws Exception
    {
        List vehiculos = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "{CALL showVeh()}";
            PreparedStatement pre = getConn().prepareCall(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Vehiculo ve = new Vehiculo();
                ve.setId(res.getInt(1));
                ve.setFechaAdquisicion(res.getString(2));
                ve.setTaller(res.getBoolean(3));
                ve.setModelo(res.getString(4));
                ve.setPlaca(res.getString(5));
                ve.setMarca(res.getString(6));
                ve.setKilometrosRecorridos(res.getDouble(7));
                ve.setFechaUltimoCambioAceite(res.getString(8));
                ve.setFechaUltimoMantenimiento(res.getString(9));
                ve.setObservaciones(res.getString(10));
                ve.setEstado(res.getInt(11));
                vehiculos.add(ve);
            }
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return vehiculos;
    }
    
    public int add(Vehiculo v) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL addVeh(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, v.getFechaAdquisicion());
            pre.setBoolean(2, v.getTaller());
            pre.setString(3, v.getModelo());
            pre.setString(4, v.getPlaca());
            pre.setString(5, v.getMarca());
            pre.setDouble(6, v.getKilometrosRecorridos());
            pre.setString(7, v.getFechaUltimoCambioAceite());
            pre.setString(8, v.getFechaUltimoMantenimiento());
            pre.setString(9, v.getObservaciones());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int edit(Vehiculo v) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL editVeh(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, v.getFechaAdquisicion());
            pre.setBoolean(2, v.getTaller());
            pre.setString(3, v.getModelo());
            pre.setString(4, v.getPlaca());
            pre.setString(5, v.getMarca());
            pre.setDouble(6, v.getKilometrosRecorridos());
            pre.setString(7, v.getFechaUltimoCambioAceite());
            pre.setString(8, v.getFechaUltimoMantenimiento());
            pre.setString(9, v.getObservaciones());
            pre.setInt(10, v.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int delete(Vehiculo v) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL deleteVeh(?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setInt(1, v.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    //</editor-fold>
}
