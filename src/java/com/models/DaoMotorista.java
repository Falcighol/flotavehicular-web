package com.models;

import com.connection.Conexion;
import com.models.Motorista;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: DaoMotorista
 * Version: 2.0
 * Fecha: 3/10/2018
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernandez
 */
public class DaoMotorista extends Conexion
{
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public List all() throws Exception
    {
        List motoristas = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "{CALL showMot()}";
            PreparedStatement pre = getConn().prepareCall(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Motorista mot = new Motorista();
                mot.setId(res.getInt(1));
                mot.setNombres(res.getString(2));
                mot.setApellidos(res.getString(3));
                mot.setEdad(res.getInt(4));
                mot.setGenero(res.getString(5));
                mot.setDui(res.getString(6));
                mot.setLicencia(res.getString(7));
                mot.setDireccion(res.getString(8));
                mot.setTelefono(res.getString(9));
                mot.setObsevaciones(res.getString(10));
                mot.setEstado(res.getInt(11));
                motoristas.add(mot);
            }
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return motoristas;
    }
    
    public int add(Motorista mot) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL addMot(?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, mot.getNombres());
            pre.setString(2, mot.getApellidos());
            pre.setInt(3, mot.getEdad());
            pre.setString(4, mot.getGenero());
            pre.setString(5, mot.getDui());
            pre.setString(6, mot.getLicencia());
            pre.setString(7, mot.getDireccion());
            pre.setString(8, mot.getTelefono());
            pre.setString(9, mot.getObsevaciones());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int edit(Motorista mot) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL editMot(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";
            PreparedStatement pre = getConn().prepareCall(sql);
            pre.setString(1, mot.getNombres());
            pre.setString(2, mot.getApellidos());
            pre.setInt(3, mot.getEdad());
            pre.setString(4, mot.getGenero());
            pre.setString(5, mot.getDui());
            pre.setString(6, mot.getLicencia());
            pre.setString(7, mot.getDireccion());
            pre.setString(8, mot.getTelefono());
            pre.setString(9, mot.getObsevaciones());
            pre.setInt(10, mot.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int delete(Motorista mot) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "{CALL deleteMot(?)}";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, mot.getId());
            if (pre.execute())
            {
                estado = 1;
            }
        } 
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    //</editor-fold>
}
