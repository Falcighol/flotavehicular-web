package com.models;

/**
 * Fecha: 13/10/2018
 * CopiRigth: Kevin Lovos
 * Nombre de la clase:Usuarios
 * Version: 1
 * @author Kevin Lovos
 */
public class Usuarios {
    private int id;
    private TipoUsuario tu;
    private String nombre;
    private String pass;
    private int estado;

    public Usuarios()
    {
        
    }

    public Usuarios(int idUsuario, TipoUsuario tipoUsuario, String nombre, String pass, int estado) {
        this.id = idUsuario;
        this.tu = tipoUsuario;
        this.nombre = nombre;
        this.pass = pass;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TipoUsuario getTu() {
        return tu;
    }

    public void setTu(TipoUsuario tu) {
        this.tu = tu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
}
