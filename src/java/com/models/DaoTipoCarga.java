package com.models;

import com.connection.Conexion;
import java.sql.*;
import java.util.*;

/**
 * Nombre de la clase: DaoTipoCarga
 * Fecha: 6/11/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Autor: Juan Pablo Elias Hernandez
 */
public class DaoTipoCarga extends Conexion
{
    public List all() throws Exception
    {
        List ls = new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql = "SELECT * FROM tiposCarga ORDER BY id DESC";
            PreparedStatement pst = this.getConn().prepareCall(sql);
            res = pst.executeQuery();
            while(res.next())
            {
                TipoCarga tc = new TipoCarga();
                tc.setId(res.getInt(1));
                tc.setDescripcion(res.getString(2));
                ls.add(tc);
            }
        }
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public void add(TipoCarga tc) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "INSERT INTO tiposCarga VALUES (NULL, ?);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, tc.getDescripcion());
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
