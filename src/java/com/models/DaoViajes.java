package com.models;

import com.connection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de la clase: DaoViajes
 * Fecha: 6/11/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Autor: Juan Pablo Elias Hernandez
 */
public class DaoViajes extends Conexion
{
    public List all() throws Exception
    {
        List ls = new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql = "{CALL showVia()}";
            PreparedStatement pst = this.getConn().prepareCall(sql);
            res = pst.executeQuery();
            while(res.next())
            {
                Viajes vi = new Viajes();
                Rutas r = new Rutas();
                Motorista m = new Motorista();
                Vehiculo veh = new Vehiculo();
                TipoCarga tc = new TipoCarga();
                Clientes c = new Clientes();
                vi.setId(res.getInt(1));
                r.setId(res.getInt(2));
                m.setId(res.getInt(3));
                veh.setId(res.getInt(4));
                tc.setId(res.getInt(5));
                c.setId(res.getInt(6));
                vi.setFechaSalida(res.getString(7));
                vi.setFechaEntrada(res.getString(8));
                vi.setCantidadCarga(res.getDouble(9));
                r.setDescripcion(res.getString(11));
                m.setDui(res.getString(12));
                m.setNombres(res.getString(13));
                veh.setPlaca(res.getString(14));
                tc.setDescripcion(res.getString(15));
                c.setNombres(res.getString(16));
                vi.setRuta(r);
                vi.setMot(m);
                vi.setVeh(veh);
                vi.setTipoCarga(tc);
                vi.setCli(c);
                ls.add(vi);
            }
        }
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public void add(Viajes v) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "INSERT INTO Viajes VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, 1);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, v.getRuta().getId());
            pst.setInt(2, v.getMot().getId());
            pst.setInt(3, v.getVeh().getId());
            pst.setInt(4, v.getTipoCarga().getId());
            pst.setInt(5, v.getCli().getId());
            pst.setString(6, v.getFechaSalida());
            pst.setString(7, v.getFechaEntrada());
            pst.setDouble(8, v.getCantidadCarga());
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void edit(Viajes v) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Viajes SET idRuta = ?, idMotorista = ?, idVehiculo = ?, " +
                "idTipoCarga = ?, idCliente = ?, fechaSalida = ?, fechaEntrada = ?, " +
                "cantidadCarga = ? WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, v.getRuta().getId());
            pst.setInt(2, v.getMot().getId());
            pst.setInt(3, v.getVeh().getId());
            pst.setInt(4, v.getTipoCarga().getId());
            pst.setInt(5, v.getCli().getId());
            pst.setString(6, v.getFechaSalida());
            pst.setString(7, v.getFechaEntrada());
            pst.setDouble(8, v.getCantidadCarga());
            pst.setInt(9, v.getId());
            pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void delete(Viajes v) throws Exception
    {
        try 
        {
            this.conectar();
            String sql ="UPDATE Viajes SET estado = 0 WHERE id = ?";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, v.getId());
            pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw  e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
