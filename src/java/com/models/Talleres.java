package com.models;

/**
 * Fecha: 13/10/2018
 * CopiRigth: Kevin Lovos
 * Nombre de la clase:Talleres
 * Version: 1
 * @author Kevin Lovos
 */
public class Talleres 
{
    private int id;
    private String nombre;
    private String direccion;
    private String tel;
    private String fallas;
    private int estado;

    public Talleres() 
    {
        
    }

    public Talleres(int idTalleres, String nombre, String direccion, String fallas, int estado) {
        this.id = idTalleres;
        this.nombre = nombre;
        this.direccion = direccion;
        this.fallas = fallas;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFallas() {
        return fallas;
    }

    public void setFallas(String fallas) {
        this.fallas = fallas;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    
}
