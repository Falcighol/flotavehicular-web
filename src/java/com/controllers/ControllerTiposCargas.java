
package com.controllers;

import com.google.gson.Gson;
import com.models.DaoTipoCarga;
import com.models.TipoCarga;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Nombre del servlet: ControllerTiposCargas
 * Fecha: 02/11/2018
 * CopyRigth: ITCA-FEPADE
 * Version: 1.0
 * @author Kevin Lovos
 */
public class ControllerTiposCargas extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoTipoCarga daoTp = new DaoTipoCarga();
        Gson jsonBuilder = new Gson();
        String action = (String) request.getParameter("action");
        try {
            if("add".equals(action))
            {
                daoTp.add(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("edit".equals(action))
            {
                // daoTp.edit(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("delete".equals(action))
            {
                TipoCarga tp = new TipoCarga();
                tp.setId(Integer.parseInt(request.getParameter("id")));
                // daoTp.delete(tp);
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("all".equals(action))
            {
               JSONObject item = new JSONObject();
               item.put("error", false);
               item.put("all", jsonBuilder.toJson(daoTp.all()));
               out.print(item.toString());
            }
        }
        catch(Exception e)
        {
            JSONObject item = new JSONObject();
            item.put("error", true);
            item.put("info", e.toString());
            out.print(item.toString());
        }
    }
    
    public TipoCarga setData(HttpServletRequest req)
    {
        TipoCarga tc = new TipoCarga();
        tc.setId(Integer.parseInt(req.getParameter("id")));
        tc.setDescripcion(req.getParameter("des"));
        return tc;
    }


   // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerMantenimientos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerMantenimientos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
