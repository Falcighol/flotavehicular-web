package com.controllers;

import com.google.gson.Gson;
import com.models.Clientes;
import com.models.DaoViajes;
import com.models.Motorista;
import com.models.Rutas;
import com.models.TipoCarga;
import com.models.Vehiculo;
import com.models.Viajes;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Nombre del servlet: ControllerTiposCargas
 * Fecha: 02/11/2018
 * CopyRigth: ITCA-FEPADE
 * Version: 1.0
 * @author Kevin Lovos
 */
public class ControllerViajes extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoViajes daoV = new DaoViajes();
        Gson jsonBuilder = new Gson();
        String action = (String) request.getParameter("action");
        try {
            if("add".equals(action))
            {
                daoV.add(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("edit".equals(action))
            {
                daoV.edit(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("delete".equals(action))
            {
                Viajes v = new Viajes();
                v.setId(Integer.parseInt(request.getParameter("id")));
                daoV.delete(v);
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("all".equals(action))
            {
               JSONObject item = new JSONObject();
               item.put("error", false);
               item.put("all", jsonBuilder.toJson(daoV.all()));
               out.print(item.toString());
            }
        }
        catch(Exception e)
        {
            JSONObject item = new JSONObject();
            item.put("error", true);
            item.put("info", e.toString());
            out.print(item.toString());
        }
    }
    
    public Viajes setData(HttpServletRequest req)
    {
        Viajes v = new Viajes();
        Rutas r = new Rutas();
        Motorista m = new Motorista();
        Vehiculo veh = new Vehiculo();
        TipoCarga tc = new TipoCarga();
        Clientes c = new Clientes();
        v.setId(Integer.parseInt(req.getParameter("id")));
        r.setId(Integer.parseInt(req.getParameter("idR")));
        m.setId(Integer.parseInt(req.getParameter("idM")));
        veh.setId(Integer.parseInt(req.getParameter("idV")));
        tc.setId(Integer.parseInt(req.getParameter("idTc")));
        c.setId(Integer.parseInt(req.getParameter("idC")));
        v.setFechaSalida(req.getParameter("fs"));
        v.setFechaEntrada(req.getParameter("fe"));
        v.setCantidadCarga(Double.parseDouble(req.getParameter("cc")));
        v.setRuta(r);
        v.setMot(m);
        v.setVeh(veh);
        v.setTipoCarga(tc);
        v.setCli(c);
        return v; 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerViajes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerViajes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
