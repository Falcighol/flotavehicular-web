
package com.controllers;

import com.google.gson.Gson;
import com.models.*;
import java.io.*;
import java.util.logging.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Nombre de la clase: ControllerMantenimientos
 * Fecha: 3/10/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * Autor: Alvaro Perez, Juan Pablo Elias Hernandez
 */
@WebServlet(name = "ControllerMantenimientos", urlPatterns = {"/ControllerMantenimientos"})
public class ControllerMantenimientos extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoMantenimientos daoM = new DaoMantenimientos();
        Gson jsonBuilder = new Gson();
        String action = (String) request.getParameter("action");
        try
        {
            if("add".equals(action))
            {
                daoM.add(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("edit".equals(action))
            {
                daoM.edit(setData(request));
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("delete".equals(action))
            {
                Mantenimientos m = new Mantenimientos();
                m.setId(Integer.parseInt(request.getParameter("id")));
                daoM.delete(m);
                JSONObject item = new JSONObject();
                item.put("error", false);
                out.print(item.toString());
            }
            else if("all".equals(action))
            {
               JSONObject item = new JSONObject();
               item.put("error", false);
               item.put("all", jsonBuilder.toJson(daoM.all()));
               out.print(item.toString());
            }
        }
        catch(Exception e)
        {
            JSONObject item = new JSONObject();
            item.put("error", true);
            item.put("info", e.toString());
            out.print(item.toString());
        }
    }
    
    public Mantenimientos setData(HttpServletRequest req)
    {
        Mantenimientos m = new Mantenimientos();
        Vehiculo v = new Vehiculo();
        Talleres t = new Talleres();
        v.setId(Integer.parseInt(req.getParameter("idV")));
        t.setId(Integer.parseInt(req.getParameter("idT")));
        m.setId(Integer.parseInt(req.getParameter("id")));
        m.setVehiculo(v);
        m.setTaller(t);
        m.setTipoMantenimiento(req.getParameter("tm"));
        m.setFechaEntrada(req.getParameter("fe"));
        m.setFechaSalida(req.getParameter("fs"));
        m.setFallaPrincipal(req.getParameter("faP"));
        m.setFallaSecundaria(req.getParameter("faS"));
        return m;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerMantenimientos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(ControllerMantenimientos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
