<%-- 
    Document   : login
    Created on : 7/11/2018, 08:58:52 PM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    HttpSession ss = request.getSession();
    if (ss.getAttribute("close") != null)
    {
        Boolean close = (Boolean) ss.getAttribute("close");
        if (close)
        {
            ss.invalidate();
        }
    }
    else if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        Integer idTu = (Integer) ss.getAttribute("tu");
        if (idTu == 1 || idTu == 2 || idTu == 3)
        {
            response.sendRedirect("index.jsp");
        }
    }
%>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Login de la aplicacion">
    <meta name="author" content="Juan Pablo Elias Hernandez">
    <title>Login</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="img/icon.png" sizes="64x64">
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
</head>
<body class="bg-dark">
    <style>
        .alert {
            display: none; 
        }
        
        @media (min-width: 576px) {
            .card-columns {
                column-count: 1;
            }
        }

        @media (min-width: 768px) {
            .card-columns {
                column-count: 2;
            }
        }

        @media (min-width: 1200px) {
            .card-columns {
                column-count: 2;
            }
        }
        .card-login {
            max-width: 30rem;
        }
    </style>
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">
                <h4><i class="fa fa-sign-in-alt"></i>&nbsp;Iniciar sesión</h4>
            </div>
            <div class="card-body">
                <form id="frm" class="needs-validation" novalidate>
                    <div class="form-group" align="center">
                        <img src="img/lock-icon.png" alt="Login" width="35%"/>
                    </div>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top: 1.5rem;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <label for="user">Usuario:</label>
                        <input class="form-control form-control-lg" id="user" name="user" type="text" 
                               aria-describedby="userHelp" placeholder="Usuario" required>
                        <div class="invalid-feedback">
                            Por favor, ingrese su usuario.
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass">Contraseña:</label>
                        <input class="form-control form-control-lg" id="pass" name="pass" type="password" 
                               placeholder="Contraseña" required>
                        <div class="invalid-feedback">
                            Por favor, ingrese su contraseña.
                        </div>
                    </div>
                    <button id="btnLogin" name="btnLogin" class="btn btn-dark btn-block btn-lg">
                        <i class="fa fa-sign-in-alt"></i>&nbsp;
                        Ingresar
                    </button>
                </form>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery-3.3.1.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/font-awesome/js/all.js"></script>
    <script src="js/jquery.mask.js" type="text/javascript"></script>
    <script src="js/sweetalert2.all.min.js" type="text/javascript"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/login.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.js"></script>
    <script>
    <%
        if (request.getSession().getAttribute("info") != null)
        {
            String info = (String) request.getSession().getAttribute("info");
    %>
        showError('<%= info %>');
    <%
        }
        request.getSession().setAttribute("info", null);
    %>
    </script>
</body>

</html>
