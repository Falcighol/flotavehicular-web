var table;
(function($) 
{   
    setInterval('all()', 20000);
    
    if ($(document).width() > 800)
    {
        $("body").toggleClass("sidenav-toggled");
        $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
        $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
    }
    
    all();
    
    table = dt();

    $("#refreshTable").on('click', function (e)
    {
        all();
    });

})(jQuery);

function all ()
{
    $.post("controllerVehiculos", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                $.each(JSON.parse(data.all), function(i, item)
                {
                    let flag = item.taller;
                    let taller = "Unknow";
                    if (flag)
                    {
                        taller = "Si";
                    }
                    else if (!flag)
                    {
                        taller = "No";
                    }
                    table.row.add([
                        item.fechaAdquisicion,
                        taller,
                        item.modelo,
                        item.placa,
                        item.marca,
                        item.kilometrosRecorridos,
                        item.fechaUltimoCambioAceite,
                        item.fechaUltimoMantenimiento,
                        item.observaciones
                    ]).draw();
                });
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
    let cd = new Date(); 
    let dt = "Ultima actualización " + cd.getDate() + "/"
            + (cd.getMonth()+1)  + "/" 
            + cd.getFullYear() + " a las "  
            + cd.getHours() + ":"  
            + cd.getMinutes() + ":" 
            + cd.getSeconds();
    $('#tableUpdateTime').html(dt);
//    console.log(dt);
}

function dt()
{
    return $("#dataTable").DataTable({
        "language": {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrando de un total de _MAX_ registros)",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "sSearch": "Buscar:",
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}