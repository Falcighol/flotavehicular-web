var table;
(function($) 
{   
    setInterval('all()', 20000);
    
    if ($(document).width() > 800)
    {
        $("body").toggleClass("sidenav-toggled");
        $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
        $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
    }
    
    all();
    
    loadSelectR();
    
    loadSelectV();
    
    loadSelectM();
    
    loadSelectTc();
    
    loadSelectC();
    
    table = dt();
    
    $('.alphaonly').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z]/g,'') ); 
    });

    $("#new").on('click', function (e)
    {
        clean();
    });

    $("#refreshTable").on('click', function (e)
    {
        all();
    });

    $("#btnAddTc").on('click', function (e)
    {
        var form = $('#frmAddTc')[0];
        var des = $('#des').val();
        addTc(form, e, des);
    });

    $("#btnAddTc2").on('click', function (e)
    {
        var form = $('#frmAddTc2')[0];
        var des = $('#des2').val();
        addTc(form, e, des);
    });
    
    $("#btnAdd").on('click', function (e)
    {
        var form = $('#frmAdd')[0];
        var idR = $('#idR').val();
        var idM = $('#idM').val();
        var idV = $('#idV').val();
        var idTc = $('#idTC').val();
        var idC = $('#idC').val();
        var fs = $('#fs').val();
        var fe = $('#fe').val();
        var cc = $('#cc').val();
        add(form, e, idR, idM, idV, idTc, idC, fs, fe, cc);
    });
    
    $("#btnEdit").on('click', function (e)
    {
        var form = $('#frmEdit')[0];
        var id = $('#idEdit').val();
        var idR = $('#idREdit').val();
        var idM = $('#idMEdit').val();
        var idV = $('#idVEdit').val();
        var idTc = $('#idTCEdit').val();
        var idC = $('#idCEdit').val();
        var fs = $('#fsEdit').val();
        var fe = $('#feEdit').val();
        var cc = $('#ccEdit').val();
        edit(form, e, id, idR, idM, idV, idTc, idC, fs, fe, cc);
    });
    
    $("#btnDel").on('click', function (e)
    {
        var id = $('#idDel').val();
        $('#deleteModal').modal('hide');
        del(id);
    });

})(jQuery);

function add (form, e, idR, idM, idV, idTc, idC, fs, fe, cc)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        $('#addModal').modal('hide');
        let datos = {
            'action' : 'add',
            'id' : 0,
            'idR' : idR,
            'idM' : idM,
            'idV' : idV,
            'idTc' : idTc,
            'idC' : idC,
            'fs' : fs,
            'fe' : fe,
            'cc' : cc
        };
        $.post("controllerViajes", datos, function (res)
        {
            var data = JSON.parse(res);
            if (!data.error)
            {
                swal({
                    title: 'Registro guardado!',
                    text: "La transaccón se ha realizado exitosamente.",
                    type: 'success'
                });
                all();
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al guardar!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
    }
    form.classList.add('was-validated');
}

function edit (form, e, id, idR, idM, idV, idTc, idC, fs, fe, cc)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        $('#editModal').modal('hide');
        swal({
            title: 'Seguro que desea editar el registro?',
            text: "Si selecciona 'Aceptar', la transaccion será realizada.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#138496',
            cancelButtonColor: '#5A6268',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value)
            {
                let datos = {
                    'action' : 'edit',
                    'id' : id,
                    'idR' : idR,
                    'idM' : idM,
                    'idV' : idV,
                    'idTc' : idTc,
                    'idC' : idC,
                    'fs' : fs,
                    'fe' : fe,
                    'cc' : cc
                };
                $.post("controllerViajes", datos, function (res)
                {
                    var data = JSON.parse(res);
                    if (!data.error)
                    {
                        swal({
                            title: 'Registro actualizado!',
                            text: "La transaccón se ha realizado exitosamente.",
                            type: 'success'
                        });
                        all();
                    }
                    else if (data.error)
                    {
                        swal({
                            title: 'Error al editar!',
                            text: "Algo salio mal en la transaccion: " + data.info,
                            type: 'error'
                        });
                    }
                });
            }
        });
    }
    form.classList.add('was-validated');
}

function del (id)
{
    swal({
        title: 'Seguro que desea eliminar el registro?',
        text: "Si selecciona 'Eliminar', la transaccion será realizada.",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#C82333',
        cancelButtonColor: '#5A6268',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            $.post("controllerViajes", 
            { 
                'action' : 'delete',
                'id' : id 
            }, 
            function (res)
            {
                var data = JSON.parse(res);
                if (!data.error)
                {
                    swal({
                        title: 'Registro eliminado!',
                        text: "La transaccón se ha realizado exitosamente.",
                        type: 'success'
                    });
                    all();
                }
                else if (data.error)
                {
                    swal({
                        title: 'Error al eliminar!',
                        text: "Algo salio mal en la transaccion: " + data.info,
                        type: 'error'
                    });
                }
            });
        }
    });
}

function all ()
{
    $.post("controllerViajes", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                $.each(JSON.parse(data.all), function(i, item)
                {
                    table.row.add([
                        item.ruta.descripcion, 
                        item.mot.nombres, 
                        item.veh.placa, 
                        item.tipoCarga.descripcion, 
                        item.cli.nombres,
                        item.fechaSalida,
                        item.fechaEntrada,
                        item.cantidadCarga,
                        `<button class="btn btn-info btn-sm" data-toggle="modal" 
                                data-target="#editModal" 
                                onclick="javascript:load(` + item.id + `,` + item.ruta.id + `, ` + item.mot.id + 
                                `, ` + item.veh.id + `, ` + item.tipoCarga.id + `, ` 
                                + item.cli.id + `, '` + item.fechaSalida + `', '` 
                                + item.fechaEntrada + `', '` + item.cantidadCarga + `')">
                            <i class="fa fa-edit"></i>
                        </button>   
                        <button class="btn btn-danger btn-sm" data-toggle="modal" 
                                data-target="#deleteModal" onclick="javascript:loadId(` + item.id + `)">
                            <i class="fas fa-fw fa-trash-alt"></i>
                        </button>`
                    ]).draw();
                });
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
    let cd = new Date(); 
    let dt = "Ultima actualización " + cd.getDate() + "/"
            + (cd.getMonth() + 1)  + "/" 
            + cd.getFullYear() + " a las "  
            + cd.getHours() + ":"  
            + cd.getMinutes() + ":" 
            + cd.getSeconds();
    $('#tableUpdateTime').html(dt);
//    console.log(dt);
}

function load(id, idR, idM, idV, idTc, idC, fs, fe, cc)
{
    $('#idEdit').val(id);
    $('#idREdit').val(idR);
    $('#idMEdit').val(idM);
    $('#idVEdit').val(idV);
    $('#idTCEdit').val(idTc);
    $('#idCEdit').val(idC);
    $('#fsEdit').val(fs);
    $('#feEdit').val(fe);
    $('#ccEdit').val(cc);
}

function loadId(id)
{
    $('#idDel').val(id);
}

function clean()
{
    $('#idR').val('');
    $('#idM').val('');
    $('#idV').val('');
    $('#idTC').val('');
    $('#idC').val('');
    $('#fs').val(moment().format('YYYY-MM-DD'));
    $('#fe').val(moment().format('YYYY-MM-DD'));
    $('#cc').val('');
}

function dt()
{
    return $("#dataTable").DataTable({
        "language": {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrando de un total de _MAX_ registros)",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "sSearch": "Buscar:",
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}

function loadSelectR ()
{
    $.post("controllerRutas", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                let select =
                    `<option class="form-control" value="">
                        -- Seleccionar --
                    </option>`;
                $.each(JSON.parse(data.all), function(i, item) {
                    select +=
                        `<option class="form-control" value="` + item.id + `"> 
                            ` + item.origen + ' - ' + item.destino + `
                        </option>`;
                });
                $('#idR').html(select);
                $('#idREdit').html(select);
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
}

function loadSelectV ()
{
    $.post("controllerVehiculos", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                let select =
                    `<option class="form-control" value="">
                        -- Seleccionar --
                    </option>`;
                $.each(JSON.parse(data.all), function(i, item) {
                    select +=
                        `<option class="form-control" value="` + item.id + `"> 
                            ` + item.placa + ` - ` + item.modelo + `
                        </option>`;
                });
                $('#idV').append(select);
                $('#idVEdit').append(select);
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
}

function loadSelectM ()
{
    $.post("controllerMotoristas", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                let select =
                    `<option class="form-control" value="">
                        -- Seleccionar --
                    </option>`;
                $.each(JSON.parse(data.all), function(i, item) {
                    select +=
                        `<option class="form-control" value="` + item.id + `"> 
                            ` + item.nombres + ` - ` + item.dui + `
                        </option>`;
                });
                $('#idM').append(select);
                $('#idMEdit').append(select);
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
}

function loadSelectC ()
{
    $.post("controllerClientes", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                let select =
                    `<option class="form-control" value="">
                        -- Seleccionar --
                    </option>`;
                $.each(JSON.parse(data.all), function(i, item) {
                    select +=
                        `<option class="form-control" value="` + item.id + `"> 
                            ` + item.nombres + `
                        </option>`;
                });
                $('#idC').append(select);
                $('#idCEdit').append(select);
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
}

function loadSelectTc ()
{
    $.post("controllerTiposCargas", { action : 'all' },
        function (res)
        {
            var data = JSON.parse(res);
            table.clear();
            if (!data.error)
            {
                let select =
                    `<option class="form-control" value="">
                        -- Seleccionar --
                    </option>`;
                $.each(JSON.parse(data.all), function(i, item) {
                    select +=
                        `<option class="form-control" value="` + item.id + `"> 
                            ` + item.descripcion + `
                        </option>`;
                });
                $('#idTC').html('');
                $('#idTCEdit').html('');
                $('#idTC').append(select);
                $('#idTCEdit').append(select);
            }
            else if (data.error)
            {
                swal({
                    title: 'Error al mostrar los registros!',
                    text: "Algo salio mal en la transaccion: " + data.info,
                    type: 'error'
                });
            }
        });
}

function addTc (form, e, des)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        let datos = {
            'action' : 'add',
            'id' : 0,
            'des' : des
        };
        $.post("controllerTiposCargas", datos, function (res)
        {
            var data = JSON.parse(res);
            if (!data.error)
            {
                showInfo('Tipo de carga agregado correctamente')
                loadSelectTc();
                
                $('#collapseExample').collapse('hide');
                $('#collapseExample2').collapse('hide');
            }
            else if (data.error)
            {
                showError('Ocurrió un error: ' + data.info)
            }
        });
    }
    form.classList.add('was-validated');
}

function showError(error)
{
    $('#error').html(error);
    $('#error').slideDown('slow');
    $('#error2').html(error);
    $('#error2').slideDown('slow');
    setTimeout(function()
    {
        $('#error2').slideUp('slow');
        $('#error2').slideUp('slow');
    }, 2000);
}

function showInfo(info)
{
    $('#info').html(info);
    $('#info').slideDown('slow');
    $('#info2').html(info);
    $('#info2').slideDown('slow');
    setTimeout(function()
    {
        $('#info').slideUp('slow');
        $('#info2').slideUp('slow');
    }, 2000);
}