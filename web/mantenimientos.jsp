<%-- 
    Document   : mantenimientos
    Created on : 3/11/2018, 10:59:15 PM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
        if (idTu == 2)
        {
            response.sendRedirect("index.jsp?denied=true");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Gestion de clientes del sistema">
    <meta name="author" content="Juan Pablo Elias Hernández">
    <title>Mantenimientos - Flota</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="img/icon.png" sizes="64x64">
    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
        <a class="navbar-brand" href="index.jsp"><i class="fas fa-truck"></i> Flota Vehicular</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link text-center" href="index.jsp">
                        <i class="fa fa-fw fa-home"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Vehiculos">
                    <a class="nav-link text-center" href="vehiculos.jsp">
                        <i class="fa fa-fw fa-car"></i>
                        <span class="nav-link-text">Vehiculos</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Motoristas">
                    <a class="nav-link text-center" href="motoristas.jsp">
                        <i class="fa fa-fw fa-users"></i>
                        <span class="nav-link-text">Motoristas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Viajes">
                    <a class="nav-link text-center" href="viajes.jsp">
                        <i class="fa fa-fw fa-luggage-cart"></i>
                        <span class="nav-link-text">Viajes</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Rutas">
                    <a class="nav-link text-center" href="rutas.jsp">
                        <i class="fa fa-fw fa-map-signs"></i>
                        <span class="nav-link-text">Rutas</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clientes">
                    <a class="nav-link text-center" href="clientes.jsp">
                        <i class="fa fa-fw fa-address-book"></i>
                        <span class="nav-link-text">Clientes</span>
                    </a>
                </li>
                <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Mantenimientos">
                    <a class="nav-link text-center" href="mantenimientos.jsp">
                        <i class="fa fa-fw fa-cogs"></i>
                        <span class="nav-link-text">Mantenimientos</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Talleres">
                    <a class="nav-link text-center" href="talleres.jsp">
                        <i class="fa fa-fw fa-wrench"></i>
                        <span class="nav-link-text">Talleres</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav sidenav-toggler">
                <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                        <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" align="center">
                    <button class="btn btn-light btn-sm" data-toggle="modal" data-target="#logoutModal">
                        <i class="fa fa-fw fa-sign-out-alt"></i> 
                        Cerrar sesión
                    </button>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Content -->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Header -->
            <div class="row">
                <div class="col-12">
                    <br>
                    <h1 align="center" class="text-dark">
                        <i class="fa fa-fw fa-address-book"></i> 
                        GESTIÓN DE MANTENIMIENTOS 
                    </h1>
                    <hr class="col-10">
                    <br>
                </div>
            </div>
            <!-- Breadcrumb -->
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.jsp">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Mantenimientos</li>
                    </ol>
                </div>
                <div class="col-sm-12 col-md-3" style="text-align: center;">
                    <button class="btn btn-outline-info mx-auto col-sm-12 col-md-8" 
                            data-toggle="modal" data-target="#addModal"
                            id="new">
                        <i class="fa fa-fw fa-plus"></i> Nuevo
                    </button>
                </div>
            </div>
            <br>
            <!-- DataTable Card -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Listado de mantenimientos
                    <button class="btn btn-light btn-sm" id="refreshTable" 
                            style="float: right;" data-toggle="tooltip" 
                            data-placement="top" title="Actualizar">
                        <i class="fa fa-fw fa-sync-alt"></i>
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col">Placa Vehiculo</th>
                                    <th scope="col">Taller</th>
                                    <th scope="col">Tipo de Mantenimiento</th>
                                    <th scope="col">Fecha Entrada</th>
                                    <th scope="col">Fecha Salida</th>
                                    <th scope="col">Falla principal</th>
                                    <th scope="col">Falla secundaria</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th scope="col">Placa Vehiculo</th>
                                    <th scope="col">Taller</th>
                                    <th scope="col">Tipo de Mantenimiento</th>
                                    <th scope="col">Fecha Entrada</th>
                                    <th scope="col">Fecha Salida</th>
                                    <th scope="col">Falla principal</th>
                                    <th scope="col">Falla secundaria</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody id="tableContent">

                            </tbody>
                        </table>
                    </div>
                </div>
            <div class="card-footer small text-muted">
                <span id="tableUpdateTime"></span>
            </div>
        </div>
        </div>
        <!-- Footer -->
        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © Flota Vehicular 2018</small>
                </div>
            </div>
        </footer>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Add Modal-->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <i class="fa fa-fw fa-plus"></i> 
                            Agregar un nuevo mantenimiento
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="frmAdd" class="frmAdd col-12 needs-validation" novalidate>
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="idV">Placa:</label>
                                    <select id="idV" class="form-control" name="idV" required>
                                        // Contenido de select
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="idT">Taller:</label>
                                    <select id="idT" class="form-control" name="idT" required>
                                        // Contenido de select
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="tm">Tipo mantenimiento:</label>
                                    <input type="text" id="tm" class="form-control"
                                           placeholder="Tipo mantenimiento" name="tm" required>
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="fe">Fecha entrada:</label>
                                    <input type="date" id="fe" class="form-control" name="fe" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="fs">Fecha salida:</label>
                                    <input type="date" id="fs" class="form-control" name="fs" required>
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="faP">Falla principal:</label>
                                    <input placeholder="Falla principal" type="text" id="faP" 
                                           class="form-control" name="faP" required>
                                </div>
                            </div>	
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="faS">Falla secundaria:</label>
                                    <input placeholder="Falla secundaria" type="text" id="faS" 
                                           class="form-control" name="faS" required>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary" data-dismiss="modal">
                            <i class="fa fa-fw fa-undo"></i> 
                            Cancelar
                        </button>
                        <button class="btn btn-outline-info" id="btnAdd">
                            <i class="fa fa-fw fa-save"></i> 
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Edit Modal-->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <i class="fa fa-fw fa-edit"></i>
                            Editar un mantenimiento existente
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" id="frmEdit" class="col-12 needs-validation" novalidate>
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="idVEdit">Placa:</label>
                                    <select id="idVEdit" class="form-control" name="idVEdit" required>
                                        // Contenido de select
                                    </select>
                                    <input type="number" id="idEdit" style="display: none;">
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="idTEdit">Taller:</label>
                                    <select id="idTEdit" class="form-control" name="idTEdit" required>
                                        // Contenido de select
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="tmEdit">Tipo mantenimiento:</label>
                                    <input type="text" id="tmEdit" class="form-control"
                                           placeholder="Tipo mantenimiento" name="tmEdit" required>
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="feEdit">Fecha entrada:</label>
                                    <input type="date" id="feEdit" class="form-control" name="feEdit" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="fsEdit">Fecha salida:</label>
                                    <input type="date" id="fsEdit" class="form-control" name="fsEdit" required>
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="faPEdit">Falla principal:</label>
                                    <input placeholder="Falla principal" type="text" id="faPEdit" 
                                           class="form-control" name="faPEdit" required>
                                </div>
                            </div>	
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label for="faSEdit">Falla secundaria:</label>
                                    <input placeholder="Falla secundaria" type="text" id="faSEdit" 
                                           class="form-control" name="faSEdit" required>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">
                            <i class="fa fa-fw fa-undo"></i>
                            Cancelar
                        </button>
                        <button class="btn btn-outline-info" id="btnEdit">
                            <i class="fa fa-fw fa-save"></i> 
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete Modal-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <i class="fa fa-fw fa-trash-alt"></i>
                            Eliminar mantenimiento
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        La eliminación de un registro es una operación delicada y este no podra ser restaurado sin la ayuda de un <b>DBA</b>.<br> 
                        Si está seguro y desea continuar, seleccione la opción <b>eliminar</b>.
                        <input type="number" id="idDel" style="display: none;">
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">
                            <i class="fa fa-fw fa-undo"></i>
                            Cancelar
                        </button>
                        <button class="btn btn-outline-danger" id="btnDel">
                            <i class="fa fa-fw fa-trash-alt"></i>
                            Eliminar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <i class="fa fa-fw fa-sign-out-alt"></i>
                            Cerrar sesión
                        </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Todos los cambios no guardados se perderan!
                        Si está seguro de cerrar la sesión actual, seleccione la opcion <b>Salir</b>.
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">
                            <i class="fa fa-fw fa-undo"></i>
                            Cancelar
                        </button>
                        <button class="btn btn-outline-dark" id="logout">
                            <i class="fa fa-fw fa-sign-out-alt"></i>
                            Salir
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery-3.3.1.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/font-awesome/js/all.js"></script>
    <script src="js/momentjs.js" type="text/javascript"></script>
    <script src="js/jquery.mask.js" type="text/javascript"></script>
    <script src="js/sweetalert2.all.min.js" type="text/javascript"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/mantenimientos.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.js"></script>
</body>
</html>

